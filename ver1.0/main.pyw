from ast import Pow
from enum import Flag
import PySimpleGUI as sg
import subprocess
from tkinter import filedialog
import os
import threading
import pyperclip


class FilePassConvert:
    def FilePass_to_FolderPass(FilePass):
        output = os.path.dirname(FilePass)
        return output

    def FilePass_to_FileName(FilePass):
        output = os.path.basename(FilePass)
        return output

    def FilePass_to_FileName_ex(FilePass):
        # output[0] : ファイル名
        # output[1] : ファイル拡張子
        FileName = FilePassConvert.FilePass_to_FileName(FilePass)
        output = os.path.splitext(FileName)
        return output


class FileSelect:
    ShortFileLinkLength = 45
    SelectingFileLink = ""

    def OpenFileSelectWindow():
        typ = [('pyファイル', '*.py'), ("pywファイル", "*.pyw")]
        dir = 'C:\\'
        fle = filedialog.askopenfilename(filetypes=typ, initialdir=dir)
        return fle

    def SetSelectingFileLink(string):
        FileSelect.SelectingFileLink = string

    def GetSelectingFileLink():
        return FileSelect.SelectingFileLink

    def ShorteningFileLink(FileLink):
        length = FileSelect.ShortFileLinkLength
        result = FileLink
        if len(FileLink) > length:
            max = len(FileLink)
            min = max - length
            result = f"...{FileLink[min:max]}"
        return result

    def FileSelect():
        this = FileSelect
        FileLink = this.OpenFileSelectWindow()  # ファイル選択ウィンドウを表示・選択させる
        this.SetSelectingFileLink(FileLink)  # 変数にファイルのフルパスを格納
        return this.ShorteningFileLink(FileLink)  # 画面表示用にパス表示を短縮


class FolderSelect:
    ShortFileLinkLength = 45
    SelectingFileLink = ""

    def OpenFolderSelectWindow():
        dir = 'C:\\'
        fld = filedialog.askdirectory(initialdir=dir)
        return fld

    def SetSelectingFileLink(string):
        FolderSelect.SelectingFileLink = string

    def GetSelectingFileLink():
        return FolderSelect.SelectingFileLink

    def ShorteningFileLink(FileLink):
        length = FolderSelect.ShortFileLinkLength
        result = FileLink
        if len(FileLink) > length:
            max = len(FileLink)
            min = max - length
            result = f"...{FileLink[min:max]}"
        return result

    def FolderSelect():
        this = FolderSelect
        FileLink = this.OpenFolderSelectWindow()  # フォルダ選択ウィンドウを表示・選択させる
        this.SetSelectingFileLink(FileLink)  # 変数にフォルダのフルパスを格納
        return this.ShorteningFileLink(FileLink)  # 画面表示用にパス表示を短縮


class IcoSelect:
    ShortIcoLinkLength = 45
    SelectingIcoLink = ""

    def OpenIcoSelectWindow():
        typ = [('icoファイル', '*.ico')]
        dir = 'C:\\'
        fle = filedialog.askopenfilename(filetypes=typ, initialdir=dir)
        return fle

    def SetSelectingIcoLink(string):
        IcoSelect.SelectingIcoLink = string

    def GetSelectingIcoLink():
        return IcoSelect.SelectingIcoLink

    def ShorteningIcoLink(IcoLink):
        length = IcoSelect.ShortIcoLinkLength
        result = IcoLink
        if len(IcoLink) > length:
            max = len(IcoLink)
            min = max - length
            result = f"...{IcoLink[min:max]}"
        return result

    def IcoSelect():
        this = IcoSelect
        IcoLink = this.OpenIcoSelectWindow()  # ファイル選択ウィンドウを表示・選択させる
        this.SetSelectingIcoLink(IcoLink)  # 変数にファイルのフルパスを格納
        return this.ShorteningIcoLink(IcoLink)  # 画面表示用にパス表示を短縮


class GetSettings:
    values = []

    def get():
        this = GetSettings
        values = this.values
        output = []
        CheckBox = this.ConvertCheckBox(values)
        PyFilePass = this.PyFilePass()
        FolderPass = this.FolderPass(CheckBox)
        IcoFilePass = this.IcoFilePass()
        output.append(PyFilePass)
        output.append(FolderPass)
        output.append(IcoFilePass)
        output.extend(CheckBox)
        return output

    def Setvalues(value):
        GetSettings.values = value

    def ConvertCheckBox(value):
        cnt = 2
        max = 7
        output = []
        while cnt <= max:
            output.append(value[cnt])
            # output[cnt - 2] = value[cnt]
            cnt = cnt + 1
        return output

    def PyFilePass():
        output = FileSelect.GetSelectingFileLink()
        return output

    def FolderPass(CheckBox):
        output = FolderSelect.GetSelectingFileLink()
        if CheckBox[5] == True:
            PyFilePass = GetSettings.PyFilePass()
            output = FilePassConvert.FilePass_to_FolderPass(PyFilePass)
        return output

    def IcoFilePass():
        output = IcoSelect.GetSelectingIcoLink()
        return output


class MakeCmdCode:
    def run():
        # WriteMode = "TextBox" : 変数に格納 + テキストボックスにコードを表示
        # WriteMode = その他 : 変数に格納
        this = MakeCmdCode
        Settings = GetSettings.get()
        Power = this.ReadyToRun(this, Settings)
        if Power[0] == True:
            return this.MakeCode(Settings)
        else:
            default = "ファイルが存在が確認できなかった項目があります。\nもう一度ファイルの選択をしてください。"
            pyFile = f"ｐｙファイル　　　：{this.OKNOConvert(Power[1])}"
            Folder = f"出力フォルダ　　　：{this.OKNOConvert(Power[2])}"
            IcoFile = f"アイコンファイル　：{this.OKNOConvert(Power[3])}"
            sg.popup(f"{default}\n\n{pyFile}\n{Folder}\n{IcoFile}", title="エラー")
            return ""

    def OKNOConvert(num):
        if num == 1:
            return "OK"
        else:
            return "Error"

    def ReadyToRun(this, Settings):
        # ファイル選択されたものが本当に存在するかの確認
        output = [True]
        FlgBox = []
        Flg = 0
        FlgBox.append(this.CheckIfFileExists(Settings[0]))  # pyファイルのパス
        FlgBox.append(this.CheckIfFileExists(Settings[1]))  # 出力フォルダのパス
        if Settings[7] == True:  # カスタムアイコンがONの場合
            FlgBox.append(this.CheckIfFileExists(Settings[2]))  # pyファイルのパス
        else:
            FlgBox.append(1)  # カスタムアイコンがOFFの場合、判別を避けるために強制的に1を代入
        Flg = FlgBox[0] * FlgBox[1] * FlgBox[2]
        output[0] = (Flg == 1)
        output.extend(FlgBox)
        return output  # すべて正常に選択されていればTrueを返す。そうでなければFalseを返す

    def CheckIfFileExists(FilePass):
        if os.path.exists(FilePass) == True:
            return 1
        else:
            return 0

    def MakeCode(Settings):
        output = "pyinstaller"
        output = f"{output} {Settings[0]}"  # pyファイル指定
        if Settings[7] == True:
            # アイコンファイル指定
            output = f"{output} --icon={Settings[2]}"
        if Settings[3] == True:
            output = f"{output} --onedir"
        if Settings[4] == True:
            output = f"{output} --onefile"
        if Settings[5] == True:
            output = f"{output} --noconsole"
        if Settings[6] == True:
            output = f"{output} --clean"
        return output


class MakeExe(threading.Thread):
    ExeLock = False

    def run(self):
        if MakeExe.ExeLock == False:
            MakeExe.ExeLock = True
            window["situation"].update("作成状況：処理中")
            Settings = GetSettings.get()
            DirectoryMove = f"cd {Settings[1]}"
            returncode = subprocess.Popen(f"{DirectoryMove}&{cmd}", shell=True)
            returncode.wait()
            window["situation"].update("作成状況：処理終了")
            MakeExe.ExeLock = False


# 画面構成
layout = [
    [sg.Text("Python exe化 補助ツール", font=('Arial', 15))],
    [sg.Text("")],
    [sg.Text("ファイル選択", font=('Arial', 15))],
    [sg.HorizontalSeparator()],
    [sg.Button("pyファイルを選択", key="FileSelect", size=(20, 1)),
     sg.Text("選択してください (必須)", key="FileSelectingDirectory", size=(77, 1)), sg.Button("フルパスを確認", key="ConfirmationFileFullPass", size=(15, 1))],
    [sg.Text("")],
    [sg.Button("出力フォルダ選択", key="FolderSelect", size=(20, 1)),
     sg.Text("選択してください (必須)", key="FolderSelectingDirectory", size=(77, 1)), sg.Button("フルパスを確認", key="ConfirmationFolderFullPass", size=(15, 1))],
    [sg.Text("", size=(20, 1)), sg.Text(
        " ※「出力フォルダをpyファイルと同じ場所にする」にチェックを入れている場合はそちらが優先")],
    [sg.Text("")],
    [sg.Button("icoファイル選択", key="IcoSelect", size=(20, 1)),
     sg.Text("選択してください (任意)", key="IcoSelectingDirectory", size=(77, 1)), sg.Button("フルパスを確認", key="ConfirmationIcoFullPass", size=(15, 1))],
    [sg.Text("")],
    [sg.Text("オプション", font=('Arial', 15))],
    [sg.HorizontalSeparator()],
    [sg.Checkbox("出力を1ディレクトリにまとめる", default=False, size=(55, 1)),
     sg.Checkbox("出力ファイルを1つにまとめる", default=False, size=(55, 1))],
    [sg.Checkbox("コンソールを表示しない", default=False, size=(55, 1)), sg.Checkbox(
        "ビルド前に前回のキャッシュ、出力ディレクトリを削除", default=True, size=(55, 1))],
    [sg.Checkbox("カスタムアイコンファイルを使用する", default=False, size=(55, 1)),
     sg.Checkbox("出力フォルダをpyファイルと同じ場所にする", default=False, size=(55, 1))],
    [sg.Text("注意：「カスタムアイコンファイルを使用する」を選択していない場合、アイコンファイルを選択していても反映されません。",
             text_color='#FFFF00', background_color="#000000", font=("bold"), size=(500, 1))],
    [sg.Text("")],
    [sg.Text("作成", font=('Arial', 15))],
    [sg.HorizontalSeparator()],
    [sg.Button("exeファイル作成", size=(20, 1), key="exeMake"),
     sg.Text("作成状況：", key="situation"), sg.Text("")],
    [sg.Button("実行コマンド確認", size=(20, 1), key="MakeCode"), sg.Input(
        "", size=(88, 1), key="Code"), sg.Button("コピー", size=(15, 1), key="Copy")]
]

# ウィンドウの生成
window = sg.Window("Python exe化 補助ツール", layout, size=(960, 600))

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    else:
        # 各種ユーザー入力データを格納
        GetSettings.Setvalues(values)

    # ボタンが押されたときの処理
    if event == "FileSelect":
        # pyファイルを選択
        fle = FileSelect.FileSelect()
        if fle == "":
            fle = "選択してください (必須)"
        window["FileSelectingDirectory"].update(fle)
    elif event == "FolderSelect":
        # 出力フォルダを選択
        fld = FolderSelect.FolderSelect()
        if fld == "":
            fld = "選択してください (必須)"
        window["FolderSelectingDirectory"].update(fld)
    elif event == "IcoSelect":
        # icoファイルを選択
        fle = IcoSelect.IcoSelect()
        if fle == "":
            fle = "選択してください (任意)"
        window["IcoSelectingDirectory"].update(fle)
    elif event == "ConfirmationFileFullPass":
        # pyファイル フルパス確認
        message = FileSelect.GetSelectingFileLink()
        if message == "":
            message = "ファイルを選択してください"
        sg.popup(message, title="ファイルのフルパス")
    elif event == "ConfirmationFolderFullPass":
        # 出力フォルダ フルパス確認
        message = FolderSelect.GetSelectingFileLink()
        if message == "":
            message = "フォルダを選択してください"
        sg.popup(message, title="フォルダのフルパス")
    elif event == "ConfirmationIcoFullPass":
        # icoファイル フルパス確認
        message = IcoSelect.GetSelectingIcoLink()
        if message == "":
            message = "アイコンファイルを選択してください"
        sg.popup(message, title="アイコンファイルのフルパス")
    elif event == "MakeCode":
        Code = MakeCmdCode.run()
        window["Code"].update(Code)
    elif event == "exeMake":
        if MakeExe.ExeLock == False:
            cmd = MakeCmdCode.run()
            if cmd != "":
                exe = MakeExe()
                exe.start()
        else:
            window["situation"].update("作成状況：exe化処理中です。しばらくお待ち下さい。")
    elif event == "Copy":
        pyperclip.copy(values["Code"])
        sg.popup_notify("実行コマンドをコピーしました。")
window.close()
